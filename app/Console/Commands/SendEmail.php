<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use RakibDevs\Weather\Weather;

class SendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:email {email?} {username?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send test email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $email = $this->argument('email');
        $username = $this->argument('username');
        /*$username = $this->ask('What is user name?');
        $email = $this->ask('What is user email?');
        while (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->error('Email is not valid! Please enter again');
            $email = $this->ask('What is user email?');
        }*/
        Mail::raw('Hi, welcome ' . $username . '!', function ($message) use ($email) {
            $message->to($email)
                ->subject("Test!");
        });
        $this->info('Email was successfully sent!');
    }
}
