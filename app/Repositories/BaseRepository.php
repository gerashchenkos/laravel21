<?php

namespace App\Repositories;

use App\Repositories\Interfaces\BaseInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use phpDocumentor\Reflection\Types\Integer;

abstract class BaseRepository implements BaseInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * @return Model
     */
    public function getModel(): Model
    {
        return $this->model;
    }

    /**
     * @param Model $model
     */
    public function setModel(Model $model): void
    {
        $this->model = $model;
    }

    public $sortBy = 'id';
    public $sortOrder = 'desc';

    /**
     * Repo Constructor
     * Override to clarify typehinted model.
     * @param Model $model Repo DB ORM Model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get all instances of model
     *
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model
            ->orderBy($this->sortBy, $this->sortOrder)
            ->get();
    }

    /**
     * Create a new record in the database
     *
     * @param array $data
     * @return model
     */
    public function create(array $data): Model
    {
        try {
            $model =  $this->model->create($data);
        } catch (Throwable $th) {
            //log here
        }
        return $model;
    }

    /**
     * Update record in the database and get data back
     *
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function update(int $id, array $data): bool
    {
        try {
            $query = $this->model->where('id', $id)->first();
            $result = $query->update($data);
        } catch (Throwable $th) {
            //log here
        }
        return $result;
    }

    /**
     * Remove record from the database
     *
     * @param int $id
     * @return boolean
     */
    public function destroy(int $id): bool
    {
        try {
            $this->model->destroy($id);
        } catch (Throwable $th) {
            //log here
        }
        return true;
    }

    /**
     * Show the record with the given id
     *
     * @param int $id
     * @return model
     */
    public function findById(int $id): Model|bool
    {
        try {
            $model = $this->model->find($id);
        } catch (Throwable $th) {
            //log here
        }
        return $model;
    }

    /**
     * Get entities count
     *
     * @return integer
     */
    public function count(): Integer
    {
        return $this->model->count();
    }
}
