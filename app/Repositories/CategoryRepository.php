<?php

namespace App\Repositories;

use App\Models\Category;
use Illuminate\Pagination\LengthAwarePaginator;

class CategoryRepository extends BaseRepository
{
    public $sortBy = 'categories.id';

    public function __construct(Category $model)
    {
        $this->model = $model;
    }
}
