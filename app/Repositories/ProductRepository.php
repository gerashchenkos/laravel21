<?php

namespace App\Repositories;

use App\Models\Product;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductRepository extends BaseRepository
{
    public $sortBy = 'products.id';

    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    public function getByCategory(int $categoryId = null): LengthAwarePaginator
    {
        if (empty($categoryId)) {
            return $this->model->paginate(9);
        } else {
            return $this->model->where("category_id", $categoryId)->paginate(9);
        }
    }
}
