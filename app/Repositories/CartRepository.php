<?php

namespace App\Repositories;

use App\Models\Cart;
use Illuminate\Pagination\LengthAwarePaginator;

class CartRepository extends BaseRepository
{
    public $sortBy = 'products.id';

    public function __construct(Cart $model)
    {
        $this->model = $model;
    }

    public function addProducts(int $id, array $productsData): bool
    {
        $cart = $this->findById($id);
        $products = [];
        $addedProducts = $cart->products->toArray();
        foreach ($addedProducts as $product) {
            if (in_array($product['id'], $productsData['products'])) {
                $cart->products()->detach($product['id']);
            }
        }
        foreach ($productsData['products'] as $product) {
            $products[$product] = ['quantity' => $productsData['quantity_' . $product]];
        }
        $cart->products()->attach($products);
        return true;
    }

    public function getTotalPrice(): float
    {
        return $this->model->products->sum(
            function ($q) {
                return $q->price * $q->pivot->quantity;
            }
        );
    }

    public function updateProducts(int $id, int $quantity)
    {
        if ($quantity == 0) {
            $this->products()->detach($id);
        } else {
            $this->products()->updateExistingPivot(
                $id,
                [
                    'quantity' => $quantity,
                ]
            );
        }
        return true;
    }
}
