<?php

namespace App\Services;

use App\Repositories\CartRepository;
use App\Models\Cart;
use Illuminate\Pagination\LengthAwarePaginator;

class CartService extends BaseService
{
    public function __construct(CartRepository $repo)
    {
        $this->repo = $repo;
    }

    public function findById(int $id): Cart
    {
        return $this->repo->findById($id);
    }

    public function getTotalPrice(): float
    {
        return $this->repo->getTotalPrice();
    }

    public function create(array $data): Cart
    {
        return $this->repo->create($data);
    }

    public function addProducts(int $id, array $data): bool
    {
        return $this->repo->addProducts($id, $data);
    }
}
