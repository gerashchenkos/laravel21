<?php

namespace App\Services;

use App\Repositories\ProductRepository;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductService extends BaseService
{
    public function __construct(ProductRepository $repo)
    {
        $this->repo = $repo;
    }

    public function getByCategory(int $categoryId = null): LengthAwarePaginator
    {
        return $this->repo->getByCategory($categoryId);
    }
}
