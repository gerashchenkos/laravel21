<?php

namespace App\Services;

use App\Repositories\CategoryRepository;
use Illuminate\Support\Collection;

class CategoryService extends BaseService
{
    public function __construct(CategoryRepository $repo)
    {
        $this->repo = $repo;
    }

    public function all(): Collection
    {
        return $this->repo->all();
    }
}
