<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Http\Requests\ProductAddRequest;
use App\Http\Requests\ProductEditRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AdminProduct extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin_product.index', [
            "products" => Product::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(
            'admin_product.create',
            [
                "categories" => Category::all()
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductAddRequest $request)
    {
        if ($files = $request->file('img')) {
            $imgName =  Auth::id() . "_" . time() . "." . $files->getClientOriginalExtension();
            $files->storeAs(
                'public/products_img',
                $imgName
            );
        }
        $data = $request->all();
        $data['img'] = $imgName;
        $product = Product::create(
            $data
        );
        return redirect()->route('admin_product.index')->with(
            'status',
            'Product #' . $product->id . ' was created!'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin_product.show', [
            "product" => Product::find($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin_product.edit', [
            "product" => Product::find($id),
            "categories" => Category::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductEditRequest $request, $id)
    {
        $data = $request->except(['_token', '_method', 'img']);
        if ($files = $request->file('img')) {
            //old file can be deleted here by getting file name from model
            //Storage::delete(storage_path('app/public/products_img') . DIRECTORY_SEPARATOR . Product::find($id)->img);
            if (file_exists(storage_path('app/public/products_img') . DIRECTORY_SEPARATOR . Product::find($id)->img)) {
                unlink(storage_path('app/public/products_img') . DIRECTORY_SEPARATOR . Product::find($id)->img);
            }
            $imgName =  Auth::id() . "_" . time() . "." . $files->getClientOriginalExtension();
            $files->storeAs(
                'public/products_img',
                $imgName
            );
            $data['img'] = $imgName;
        }
        Product::updateData($id, $data);
        return redirect()->route('admin_product.index')->with(
            'status',
            'Product #' . $id . ' was updated!'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
