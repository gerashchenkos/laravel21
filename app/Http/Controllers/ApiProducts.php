<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Resources\ProductCollection;

class ApiProducts extends Controller
{
    public function index(Request $request)
    {
        return new ProductCollection(Product::all());
    }
}
