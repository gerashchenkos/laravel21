<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Cart;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use App\Services\ProductService;
use App\Services\CategoryService;

class Product extends Controller
{
    public function __construct(ProductService $products, CategoryService $categories)
    {
        $this->products = $products;
        $this->categories = $categories;
    }

    public function index(Request $request, int $categoryId = null)
    {
        return view('product.index', [
            'products' => $this->products->getByCategory($categoryId),
            'categories' => $this->categories->all()
        ]);
    }

    public function add(Request $request)
    {
        return view('product.add', [
            'product' => $request->all()
        ]);
    }
}
