<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\City;
use Dnsimmons\OpenWeather\OpenWeather;
use App\Models\WeatherCity;
use Carbon\Carbon;
use App\Http\Requests\WeatherStoreRequest;

class Weather extends Controller
{
    public function index(Request $request)
    {
        return view(
            'weather.index',
            [
                'cities' => City::all()
            ]
        );
    }

    public function store(WeatherStoreRequest $request)
    {
        /*$validated = $request->validate([
            'city_id' => 'required|exists:cities,name',
        ]);*/

        $city = City::find($request->city_id);
        $wt = new OpenWeather();
        $info = $wt->getCurrentWeatherByCityName($city->name, 'metric');
        WeatherCity::createIfNotExist(
            $request->city_id,
            $info['forecast']['temp'],
            $info['forecast']['pressure'],
            $info['forecast']['humidity'],
            $info['condition']['desc']
        );
        return redirect()->route('weather.city', ["city" => $request->city_id]);
    }

    public function city(City $city)
    {
        return view(
            'weather.city',
            [
                'weather' => WeatherCity::getWeatherByCityDate($city->id),
                //'city' => $city
            ]
        );
    }
}
