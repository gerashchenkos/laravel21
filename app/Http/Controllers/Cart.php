<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CartAddRequest;
use App\Models\Cart as CartModel;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CartUpdateRequest;
use App\Services\CartService;

class Cart extends Controller
{
    public function __construct(CartService $cart)
    {
        $this->cart = $cart;
    }

    public function index(Request $request)
    {
        $cart = $this->cart->findById(session('cart_id'));
        return view('cart.index', [
            'products' => $cart->products,
            'total_price' => $this->cart->getTotalPrice(),
        ]);
    }

    public function add(CartAddRequest $request)
    {
        if (empty(session('cart_id'))) {
            $cart = $this->cart->create([
                'user_id' => Auth::id()
            ]);
            session(['cart_id' => $cart->id]);
        } else {
            $cart = $this->cart->findById(session('cart_id'));
        }
        $this->cart->addProducts($cart->id, $request->all());
        return redirect()->route('cart');
    }

    public function update(CartUpdateRequest $request)
    {
        $cart = CartModel::find(session('cart_id'));
        if (!empty($cart)) {
            $cart->updateProducts($request->except('_token'));
        }
        return redirect()->route('cart');
    }
}
