<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cart;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\OrderCreateRequest;
use App\Models\Order as OrderModel;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use App\Models\OrderPaymentsLog;
use Mpdf\Mpdf;
use Illuminate\Support\Facades\Log;

class Order extends Controller
{
    public function index(Request $request)
    {
        if (!empty(session('cart_id'))) {
            $cart = Cart::find(session('cart_id'));
            return view(
                'order.index',
                [
                    'cart' => $cart,
                    'total_price' => $cart->getTotalPrice(),
                    'user' => Auth::user(),
                    'tax' => config('settings.tax_perc')
                ]
            );
        } else {
            return redirect()->route('products');
        }
    }

    public function export(Request $request)
    {
        Log::info('test!');
        $cart = Cart::find(session('cart_id'));
        $mpdf = new Mpdf();
        $html = view(
            'order.export',
            [
                'cart' => $cart,
                'total_price' => $cart->getTotalPrice(),
                'user' => Auth::user(),
                'tax' => config('settings.tax_perc')
            ]
        )->render();
        $mpdf->WriteHTML($html);
        $mpdf->Output('order.pdf', "D");
    }

    public function create(OrderCreateRequest $request)
    {
        if (empty(session('cart_id'))) {
            return redirect()->route('products');
        }
        $cart = Cart::find(session('cart_id'));
        $totalPrice = $cart->getTotalPrice() + $cart->getTotalPrice() * config('settings.tax_perc') / 100;
        $order = OrderModel::create(
            [
                "cart_id" => session('cart_id'),
                "total_price" => $totalPrice
            ]
        );
        $result = $order->pay($request->all(), $totalPrice);
        if ($result['success']) {
            return redirect()->route('order.thankyou', ["order" => $order->id]);
        } else {
            return back()->withErrors($result['message'])->withInput();
        }
    }

    public function thankyou(\App\Models\Order $order)
    {
        return view(
            'order.thankyou',
            [
                'order' => $order,
            ]
        );
    }
}
