<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use LVR\CreditCard\CardCvc;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;

class OrderCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'card_num' => ['required', new CardNumber()],
            'year' => ['required', new CardExpirationYear($this->get('month'))],
            'month' => ['required', new CardExpirationMonth($this->get('year'))],
            'cvv' => ['required', new CardCvc($this->get('card_num'))]
        ];
    }
}
