<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'required',
                'max:120',
                'string',
            ],
            'sku' => [
                'required',
                'digits_between:5,20',
                'numeric',
                Rule::unique('products')->ignore($this->route('admin_product'))
            ],
            'category_id' => [
                'required',
                'exists:categories,id'
            ],
            'description' => [
                'required',
                'string',
                'max:1000',
            ],
            'price' => [
                'required',
                'numeric',
                'min:1',
                'max:100000'
            ],
            'quantity' => [
                'required',
                'digits_between:1,1000000',
            ],
            'img' => [
                'nullable',
                'image',
                'mimes:jpg,bmp,png,jpeg',
                'max:20480'
            ],
        ];
    }
}
