<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Pagination\LengthAwarePaginator;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['sku', 'category_id', 'title', 'description', 'price', 'quantity'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /*public static function getByCategory(int $categoryId = null): LengthAwarePaginator
    {
        if (empty($categoryId)) {
            return Product::paginate(9);
        } else {
            return Product::where("category_id", $categoryId)->paginate(9);
        }
    }

    public static function updateData(int $id, array $data)
    {
        return Product::where('id', $id)
            ->update($data);
    }*/
}
