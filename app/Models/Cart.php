<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cart';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id'];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'cart_products', 'cart_id', 'product_id')->withPivot('quantity')->withTimestamps();
    }

    /*public function addProducts(array $productsData)
    {
        $products = [];
        $addedProducts = $this->products->toArray();
        foreach ($addedProducts as $product) {
            if (in_array($product['id'], $productsData['products'])) {
                $this->products()->detach($product['id']);
            }
        }
        foreach ($productsData['products'] as $product) {
            $products[$product] = ['quantity' => $productsData['quantity_' . $product]];
        }
        $this->products()->attach($products);
        return true;
    }

    public function getTotalPrice(): float
    {
        return $this->products->sum(
            function ($q) {
                return $q->price * $q->pivot->quantity;
            }
        );
    }

    public function updateProducts(array $productsData)
    {
        $products = [];
        foreach ($productsData as $key => $val) {
            $id = explode("quantity_", $key)[1] ?? 0;
            if ($id) {
                if ($val == 0) {
                    $this->products()->detach($id);
                } else {
                    $this->products()->updateExistingPivot(
                        $id,
                        [
                            'quantity' => $val,
                        ]
                    );
                }
            }
        }
        return true;
    } */
}
