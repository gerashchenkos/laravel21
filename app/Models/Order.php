<?php

namespace App\Models;

use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Order extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cart_id', 'total_price'];

    public function pay(array $cardData, float $totalPrice)
    {
        $user = Auth::user();
        if (empty($user->stripe_customer_id)) {
            $customer = Stripe::customers()->create(
                [
                    'email' => $user->email,
                ]
            );
            $user->stripe_customer_id = $customer['id'];
            $user->save();
        }
        try {
            $token = Stripe::tokens()->create(
                [
                    'card' => [
                        'number' => $cardData['card_num'],
                        'exp_month' => $cardData['month'],
                        'cvc' => $cardData['cvv'],
                        'exp_year' => $cardData['year'],
                    ],
                ]
            );
            $card = Stripe::cards()->create($user->stripe_customer_id, $token['id']);
            $charge = Stripe::charges()->create(
                [
                    'customer' => $user->stripe_customer_id,
                    'currency' => 'USD',
                    'amount' => $totalPrice,
                    'source' => $card['id'],
                    'capture' => true
                ]
            );
            if (!empty($charge['id'])) {
                $this->status = 'paid';
                $this->save();
                $status = [];
                if ($charge['captured']) {
                    $status[] = 'captured';
                }
                if ($charge['paid']) {
                    $status[] = 'paid';
                }
                OrderPaymentsLog::create(
                    [
                        "order_id" => $this->id,
                        "status" => implode("|", $status),
                        "response" => json_encode($charge)
                    ]
                );
                session(['cart_id' => null]);
                return [
                    "success" => 1,
                    "order" => $this->id
                ];
            }
        } catch (
        \Cartalyst\Stripe\Exception\StripeException $e
        ) {
            OrderPaymentsLog::create(
                [
                    "order_id" => $this->id,
                    "status" => $e->getErrorType(),
                    "response" => json_encode(
                        [
                            "type" => $e->getErrorType(),
                            "code" => $e->getErrorCode(),
                            "message" => $e->getMessage(),
                        ]
                    )
                ]
            );
            return [
                "success" => 0,
                "message" => $e->getMessage()
            ];
        }
    }
}
