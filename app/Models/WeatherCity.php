<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class WeatherCity extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'weather_city';

    protected $fillable = ['city_id', 'temp', 'pressure', 'humidity', 'description'];

    public static function createIfNotExist(int $cityId, float $temp, int $pressure, int $humidity, string $desc): WeatherCity
    {
        $weather = WeatherCity::where(
            [
                ['city_id', '=', $cityId],
                ['temp', '=', $temp],
                ['pressure', '=', $pressure],
                ['humidity', '=', $humidity]
            ]
        )->whereDate('created_at', Carbon::now()->format('Y-m-d'))->first();
        if (!empty($weather)) {
            return $weather;
        }
        return WeatherCity::create(
            [
                "city_id" => $cityId,
                "temp" => $temp,
                "pressure" => $pressure,
                "humidity" => $humidity,
                "description" => $desc
            ]
        );
    }

    public static function getWeatherByCityDate(int $cityId, string $date = null): WeatherCity
    {
        $date = $date ?? Carbon::now()->format('Y-m-d');
        return WeatherCity::where('city_id', $cityId)->whereDate(
            'created_at',
            $date
        )->latest()->first();
    }

    /**
     * Get the phone associated with the user.
     */
    public function city()
    {
        //return $this->belongsTo(City::class);
        //return $this->hasOne(City::class, 'id', 'city_id');
        return $this->belongsTo(City::class);
    }
}
