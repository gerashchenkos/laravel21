@extends('layouts.shop')

@section('content')
    <section class="py-5 text-center container">
        <div class="row py-lg-5">
            <div class="col-lg-6 col-md-8 mx-auto">
                <h1 class="fw-light">Shopping Cart example</h1>
                <p class="lead text-muted">PHP Shopping Cart</p>
                <p>
                    <a href="{{ route('products') }}" class="btn btn-primary my-2">Products Page</a>
                    <a href="{{ route('cart') }}" class="btn btn-secondary my-2">Cart Page</a>
                </p>
            </div>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="POST" action="{{ route('cart.add') }}">
                @csrf
                <div class="row">
                    <div class="col-2">
                        @foreach($categories as $category)
                        <p><a href="{{ route('products', ["category_id" => $category->id]) }}">{{ $category->title }}</a></p>
                        @endforeach
                    </div>
                    <div class="col-10">
                        <div class="row">
                            @foreach($products as $product)
                            <div class="col-4">
                                <div class="card shadow-sm">
                                    @if(empty($product->img))
                                        <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em">{{ $product->title }}</text></svg>
                                    @else
                                        <img class="bd-placeholder-img card-img-top" width="100%" height="225" src="/storage/products_img/{{ $product->img }}">
                                    @endif
                                    <div class="card-body">
                                        <p class="card-text desc-block">{{ $product->description}}</p>
                                        <p class="card-text">${{ $product->price}}</p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group">
                                                <input type="checkbox" name="products[]" value="{{ $product->id }}">
                                            </div>
                                            <small class="text-muted">Quantity: <input type="number" value="1" name="quantity_{{ $product->id }}" min="1" max="{{ $product->quantity }}"></small>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <br>
                <div class="d-flex justify-content-center">
                    <button type="submit" class="btn btn-primary">Buy</button>
                </div>
                <br>
                <div class="d-flex justify-content-center">
                    {{ $products->links() }}
                </div>
            </form>
        </div>
    </div>
@endsection

