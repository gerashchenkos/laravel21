<html>
    <head>
    </head>
    <body>

    <div class="album py-5 bg-light">
        <div class="container">
                <div class="invoice p-3 mb-3">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-12">
                            <h4>
                                <i class="fas fa-globe"></i> Best Shop Inc
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small style="float: right !important;" class="float-right">Date: {{ date('d-m-Y') }}</small>
                            </h4>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            To
                            <address>
                                <strong>{{ $user->name }}</strong><br>
                                Phone: (555) 539-1037<br>
                                Email: {{ $user->email }}
                            </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            <b>Invoice #{{ $cart->id }}</b><br>
                            <br>
                            <b>Payment Due:</b> {{ date('d-m-Y', strtotime("+1 day")) }}<br>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Qty</th>
                                    <th>Product</th>
                                    <th>SKU</th>
                                    <th>Description</th>
                                    <th>Subtotal</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cart->products as $product)
                                <tr>
                                    <td>{{ $product->pivot->quantity }}</td>
                                    <td>{{ $product->title }}</td>
                                    <td>{{ $product->sku }}</td>
                                    <td>{{ \Illuminate\Support\Str::limit($product->description, 100, $end='...') }}</td>
                                    <td>${{ $product->pivot->quantity *  $product->price}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-6">
                            <p class="lead">Payment Methods:</p>
                            <img src="https://adminlte.io/themes/v3/dist/img/credit/visa.png" alt="Visa">
                            <img src="https://adminlte.io/themes/v3/dist/img/credit/mastercard.png" alt="Mastercard">
                            <img src="https://adminlte.io/themes/v3/dist/img/credit/american-express.png" alt="American Express">
                            <img src="https://adminlte.io/themes/v3/dist/img/credit/paypal2.png" alt="Paypal">

                            <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya
                                handango imeem
                                plugg
                                dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                            </p>
                        </div>
                        <!-- /.col -->
                        <div class="col-6">
                            <p class="lead">Amount Due 2/22/2014</p>

                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th style="width:50%">Subtotal:</th>
                                        <td>${{ $total_price }}</td>
                                    </tr>
                                    <tr>
                                        <th>Tax ({{$tax}}%)</th>
                                        <td>${{ $total_price * $tax / 100 }}</td>
                                    </tr>
                                    <tr>
                                        <th>Total:</th>
                                        <td>${{ $total_price + $total_price * $tax / 100 }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
        </div>
    </div>
    </body>
</html>
