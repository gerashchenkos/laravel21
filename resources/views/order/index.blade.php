@extends('layouts.shop')

@section('content')
    <section class="py-5 text-center container">
        <div class="row py-lg-5">
            <div class="col-lg-6 col-md-8 mx-auto">
                <h1 class="fw-light">Shopping Cart example</h1>
                <p class="lead text-muted">PHP Shopping Cart</p>
                <p>
                    <a href="{{ route('products') }}" class="btn btn-secondary my-2">Products Page</a>
                    <a href="{{ route('cart') }}" class="btn btn-secondary my-2">Cart Page</a>
                </p>
            </div>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">
                <div class="invoice p-3 mb-3">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-12">
                            <h4>
                                <i class="fas fa-globe"></i> Best Shop Inc
                                <small class="float-right">Date: {{ date('d-m-Y') }}</small>
                            </h4>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            To
                            <address>
                                <strong>{{ $user->name }}</strong><br>
                                Phone: (555) 539-1037<br>
                                Email: {{ $user->email }}
                            </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            <b>Invoice #{{ $cart->id }}</b><br>
                            <br>
                            <b>Payment Due:</b> {{ date('d-m-Y', strtotime("+1 day")) }}<br>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Qty</th>
                                    <th>Product</th>
                                    <th>SKU</th>
                                    <th>Description</th>
                                    <th>Subtotal</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cart->products as $product)
                                <tr>
                                    <td>{{ $product->pivot->quantity }}</td>
                                    <td>{{ $product->title }}</td>
                                    <td>{{ $product->sku }}</td>
                                    <td>{{ \Illuminate\Support\Str::limit($product->description, 100, $end='...') }}</td>
                                    <td>${{ $product->pivot->quantity *  $product->price}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-6">
                            <p class="lead">Payment Methods:</p>
                            <img src="https://adminlte.io/themes/v3/dist/img/credit/visa.png" alt="Visa">
                            <img src="https://adminlte.io/themes/v3/dist/img/credit/mastercard.png" alt="Mastercard">
                            <img src="https://adminlte.io/themes/v3/dist/img/credit/american-express.png" alt="American Express">
                            <img src="https://adminlte.io/themes/v3/dist/img/credit/paypal2.png" alt="Paypal">

                            <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya
                                handango imeem
                                plugg
                                dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                            </p>
                        </div>
                        <!-- /.col -->
                        <div class="col-6">
                            <p class="lead">Amount Due 2/22/2014</p>

                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th style="width:50%">Subtotal:</th>
                                        <td>${{ $total_price }}</td>
                                    </tr>
                                    <tr>
                                        <th>Tax ({{$tax}}%)</th>
                                        <td>${{ $total_price * $tax / 100 }}</td>
                                    </tr>
                                    <tr>
                                        <th>Total:</th>
                                        <td>${{ $total_price + $total_price * $tax / 100 }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- this row will not appear when printing -->
                    <div class="row no-print">
                        <div class="col-12">
                            <a href="invoice-print.html" rel="noopener" target="_blank" class="btn btn-default"><i
                                    class="fas fa-print"></i> Print</a>
                            <a type="button" class="btn btn-primary float-right" href="{{ route('order.export') }}" style="margin-right: 5px;">
                                <i class="fas fa-download"></i> Generate PDF
                            </a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-header">
                                    <strong>Credit Card</strong>
                                    <small>enter your card details</small>
                                </div>
                                <div class="card-body">
                                    <form method="POST" action="{{ route('cart.create') }}">
                                        @csrf
                                    @if(Session::has('errors'))
                                        <div class="alert alert-danger">
                                            {{Session::get('errors')->first()}}
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}"
                                                       id="name" type="text" placeholder="Enter your name">
                                                @error('name')
                                                <br>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="ccnumber">Credit Card Number</label>
                                                <div class="input-group">
                                                    <input class="form-control @error('card_num') is-invalid @enderror" value="{{ old('card_num') }}" name="card_num" type="text" placeholder="0000 0000 0000 0000" autocomplete="email">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="mdi mdi-credit-card"></i>
                                                        </span>
                                                    </div>
                                                    @error('card_num')
                                                    <br>
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-4">
                                            <label for="ccmonth">Month</label>
                                            <select class="form-control @error('month') is-invalid @enderror" name="month" id="ccmonth">
                                                @for($i = 1; $i <=12; $i++)
                                                <option @if(old('month') && old('month') == $i) selected @endif value="{{ $i }}">{{ $i }}</option>
                                                @endfor
                                            </select>
                                            @error('month')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label for="ccyear">Year</label>
                                            <select name="year" class="form-control @error('year') is-invalid @enderror" id="ccyear">
                                                @for($i = date('Y'); $i <= (date('Y') + 10); $i++)
                                                    <option @if(old('year') && old('year') == $i) selected @endif value="{{ $i }}">{{ $i }}</option>
                                                @endfor
                                            </select>
                                            @error('year')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="cvv">CVV/CVC</label>
                                                <input class="form-control @error('cvv') is-invalid @enderror" name="cvv" id="cvv" type="text" placeholder="123">
                                                @error('cvv')
                                                <br>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <input type="submit" class="btn btn-success float-right" value="Submit Payment"><i class="far fa-credit-card"></i>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection

