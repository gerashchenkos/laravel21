@extends('layouts.shop')

@section('content')
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fas fa-check"></i> Thank you!</h5>
        You Payment was success! Track your order by # {{ $order->id }}
    </div>
@endsection

