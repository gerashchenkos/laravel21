@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Product Details</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <h3 class="d-inline-block d-sm-none">{{ $product->title }}</h3>
                            <div class="col-12">
                                <img src="/storage/products_img/{{ $product->img }}" class="product-image" alt="Product Image">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <h3 class="my-3">{{ $product->title }}</h3>
                            <p>{{ $product->description }}</p>

                            <hr>
                            <h4 class="mt-3">Quanity: {{ $product->quantity }}</h4>
                            <h4 class="mt-3">Category: {{ $product->category->title }}</h4>

                            <div class="bg-gray py-2 px-3 mt-4">
                                <h2 class="mb-0">
                                    ${{ $product->price }}
                                </h2>
                                <h4 class="mt-0">
                                    <small>SKU: {{ $product->sku }} </small>
                                </h4>
                            </div>

                            <div class="mt-4">
                                <a class="btn btn-primary btn-lg btn-flat" href="{{ route('admin_product.index') }}">Back</a>

                                <a class="btn btn-default btn-lg btn-flat" href="{{ route('admin_product.edit', ["admin_product" => $product->id]) }}">
                                    Edit
                                </a>
                            </div>


                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection

@section('js')
@endsection
