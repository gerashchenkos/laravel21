@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Edit Product</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="POST" action="{{ route('admin_product.update', ["admin_product" => $product->id]) }}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputTtitle">Product Title</label>
                            <input type="text" class="form-control @error('title') is-invalid @enderror"
                                   value="{{ old('title') ?? $product->title }}" name="title" id="exampleInputTtitle"
                                   placeholder="Enter title">
                            @error('title')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputSku">Sku</label>
                            <input type="text" class="form-control @error('sku') is-invalid @enderror"
                                   value="{{ old('sku') ?? $product->sku }}" name="sku" id="exampleInputSku" placeholder="Enter Sku">
                            @error('sku')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputDescription">Description</label>
                            <textarea class="form-control @error('description') is-invalid @enderror" name="description"
                                      id="exampleInputDescription"
                                      placeholder="Enter Description">{{ old('description') ?? $product->description }}</textarea>
                            @error('description')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPrice">Price</label>
                            <input type="text" class="form-control @error('price') is-invalid @enderror"
                                   value="{{ old('price') ?? $product->price }}" name="price" id="exampleInputPrice" placeholder="Enter price">
                            @error('price')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputQuantity">Quantity</label>
                            <input type="number" min="1" class="form-control @error('quantity') is-invalid @enderror"
                                   value="{{ old('quantity') ?? $product->quantity }}" name="quantity" id="exampleInputQuantity" placeholder="Enter Quantity">
                            @error('quantity')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputCategory">Category</label>
                            <select name="category_id" id="exampleInputCategory" class="form-control @error('category_id') is-invalid @enderror">
                                <option>Please select Category</option>
                                @foreach($categories as $category)
                                    <option @if((old('category_id') && old('category_id') == $category->id) || $category->id == $product->category_id) selected @endif value="{{ $category->id }}">{{ $category->title }}</option>
                                @endforeach
                            </select>
                            @error('category_id')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Current Product Image</label>
                            <div class="col-4">
                                <img src="/storage/products_img/{{ $product->img }}" class="product-image" alt="Product Image">
                            </div>
                            <label for="exampleInputFile">Upload New Product Image</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" name="img" class="custom-file-input @error('img') is-invalid @enderror" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Upload</span>
                                </div>
                            </div>
                            @error('img')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection

@section('js')
@endsection
