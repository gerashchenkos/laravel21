@extends('layouts.shop')

@section('content')
    <section class="py-5 text-center container">
        <div class="row py-lg-5">
            <div class="col-lg-6 col-md-8 mx-auto">
                <h1 class="fw-light">Shopping Cart example</h1>
                <p class="lead text-muted">PHP Shopping Cart</p>
                <p>
                    <a href="{{ route('products') }}" class="btn btn-secondary my-2">Products Page</a>
                    <a href="{{ route('cart') }}" class="btn btn-primary my-2">Cart Page</a>
                </p>
            </div>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="POST" action="{{ route('cart.update') }}">
                @csrf
                <div class="row">
                    <div class="col-10">
                        <div class="row">
                            @foreach($products as $product)
                            <div class="col-4">
                                <div class="card shadow-sm">
                                    <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em">{{ $product->title }}</text></svg>

                                    <div class="card-body">
                                        <p class="card-text desc-block">{{ $product->description}}</p>
                                        <p class="card-text">Product Total: <br> ${{ $product->price * $product->pivot->quantity}} ({{ $product->price . ' X '. $product->pivot->quantity}})</p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group">
                                            </div>
                                            <small class="text-muted">Selected Quantity: <input type="number" value="{{ $product->pivot->quantity }}" name="quantity_{{ $product->id }}" min="0" max="{{ $product->quantity }}"></small>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <br>
                <div class="d-flex justify-content-center">
                    <button type="submit" class="btn btn-primary">Update</button>&nbsp;&nbsp;
                    <a class="btn btn-primary" href="{{ route('order') }}">Next</a>
                </div>
                <br>
            </form>
                <h3>Total Price: ${{ $total_price }}</h3>
        </div>
    </div>
@endsection

