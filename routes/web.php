<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Product;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\Weather;
use App\Http\Controllers\Cart;
use App\Http\Controllers\AdminCategory;
use App\Http\Controllers\AdminProduct;
use App\Http\Controllers\Order;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome', ["message" => "Hello!"]);

Route::middleware(['auth'])->group(function () {
    Route::prefix('products')->group(function () {
        Route::get('/{category_id?}', [Product::class, 'index'])->name('products')->whereNumber("category_id");
        Route::post('/add', [Product::class, 'add'])->name('products.add');
    });

    Route::prefix('cart')->group(function () {
        Route::get('/', [Cart::class, 'index'])->name('cart');
        Route::post('/add', [Cart::class, 'add'])->name('cart.add');
        Route::post('/update', [Cart::class, 'update'])->name('cart.update');
    });

    Route::prefix('order')->group(function () {
        Route::get('/', [Order::class, 'index'])->name('order');
        Route::post('/create', [Order::class, 'create'])->name('cart.create');
        Route::get('/thankyou/{order}', [Order::class, 'thankyou'])->name('order.thankyou');
        Route::get('/export', [Order::class, 'export'])->name('order.export');
    });

    Route::middleware(['admin.check:admin'])->group(function () {
        Route::resource('admin_category', AdminCategory::class);
        Route::resource('admin_product', AdminProduct::class);
    });
});

Route::resource('photos', PhotoController::class);

Route::prefix('weather')->group(function () {
    Route::get('/', [Weather::class, 'index'])->name('weather');
    Route::post('/store', [Weather::class, 'store'])->name('weather.store');
    Route::get('/city/{city}', [Weather::class, 'city'])->name('weather.city');
});

Auth::routes();
